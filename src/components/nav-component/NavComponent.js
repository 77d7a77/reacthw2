import React from 'react';
import styled from 'styled-components';
import { AiOutlineDelete } from 'react-icons/ai';
const NavComponentStyle = styled.nav`
  background-color: rgba(200, 0, 255, 1);
  .nav__list {
    display: flex;
    justify-content: space-around;
    list-style: none;
    margin:0;
  }
  .nav__item {
    text-align: center;
    padding: 14px 16px;
  }
  a {
    color: black;
    cursor:pointer;
    text-decoration:none;
  }
`;

function NavComponent(){
  const total = 0
    return (
    <NavComponentStyle className='page__nav'>
      <ul className='nav__list'>
        <li className='nav__item'>
          <a href='#'>GAMES</a>
        </li>
        <li className='nav__item'>
          <a href='#'>FAVORITES ({total})</a>
          
        </li>
        <li className='nav__item'>
        <a  href='#'><AiOutlineDelete/> ({total})</a>
        </li>
      </ul>
    </NavComponentStyle>
  );
}

export default NavComponent;