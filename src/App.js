import React from 'react';
import ProductList from './components/product-list/Product-list.js';
import styled from 'styled-components';
import NavComponent from './components/nav-component/NavComponent.js';
const AppComponent = styled.div`
  min-width: 1200px;
  margin: 0;
  padding: 0;
  background-color: rgba(81, 44, 91, 1);
`;
function App() {
  return (
    <AppComponent>
      <NavComponent/>
      <ProductList/>
    </AppComponent>
      
  );
}

export default App;